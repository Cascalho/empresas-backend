'use strict';
module.exports = (sequelize, DataTypes) => {
  const Enterprise = sequelize.define(
    'Enterprise',
    {
      email_enterprise: DataTypes.STRING,
      facebook: DataTypes.STRING,
      twitter: DataTypes.STRING,
      linkedin: DataTypes.STRING,
      phone: DataTypes.STRING,
      own_enterprise: DataTypes.BOOLEAN,
      enterprise_name: DataTypes.STRING,
      photo: DataTypes.TEXT,
      description: DataTypes.TEXT,
      city: DataTypes.STRING,
      country: DataTypes.STRING,
      value: DataTypes.INTEGER,
      share_price: DataTypes.INTEGER,
      enterprise_type_id: DataTypes.INTEGER,
      enterprise_type_name: DataTypes.STRING
    },
    {
      hooks: {
        beforeCreate: enterprise => {
          enterprise.city = enterprise.city.toLowerCase();
          enterprise.country = enterprise.country.toLowerCase();
          enterprise.enterprise_name = enterprise.enterprise_name.toLowerCase();
          enterprise.enterprise_type_name = enterprise.enterprise_type_name.toLowerCase();
        }
      }
    }
  );
  Enterprise.associate = function(models) {};
  return Enterprise;
};
