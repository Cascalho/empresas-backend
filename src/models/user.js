'use strict';

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      investor_name: DataTypes.STRING,
      email: DataTypes.STRING,
      city: DataTypes.STRING,
      country: DataTypes.STRING,
      balance: DataTypes.INTEGER,
      photo: DataTypes.STRING,
      first_access: DataTypes.BOOLEAN,
      super_angel: DataTypes.BOOLEAN,
      portfolio_value: DataTypes.INTEGER,
      enterprises_number: DataTypes.INTEGER,
      enterprises: DataTypes.STRING,
      enterprise: DataTypes.STRING,
      password_hash: DataTypes.STRING
    },
    {}
  );
  User.associate = function(models) {};
  return User;
};
