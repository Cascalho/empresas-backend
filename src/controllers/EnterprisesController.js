const EnterprisesRepository = require('../repositories/EnterprisesRepository');

module.exports = {
  index: async (req, res, next) => {
    try {
      let enterprises;
      if (Object.keys(req.query).length !== 0)
        enterprises = await EnterprisesRepository.filter(req.query);

      if (!enterprises) enterprises = await EnterprisesRepository.getAll();

      return res.status(200).json({ enterprises: enterprises });
    } catch (error) {
      return next(error);
    }
  },

  show: async (req, res, next) => {
    try {
      const enterprise = await EnterprisesRepository.getOne(req.params.id);
      if (!enterprise)
        return res.status(404).json({ status: '404', error: 'Not Found' });

      return res.status(200).json({ enterprise: enterprise, success: true });
    } catch (error) {
      return next(error);
    }
  }
};
