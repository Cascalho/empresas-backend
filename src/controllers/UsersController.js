const { validationResult } = require('express-validator');
const { login } = require('../services/auth');
const UsersRepository = require('../repositories/UsersRepository');

const resHeaders = (uid, client, token) => {
  return {
    uid: uid,
    client: client,
    'access-token': token
  };
};

const resBody = (investor, enterprise) => {
  return {
    investor: investor,
    enterprise: enterprise,
    success: true
  };
};

module.exports = {
  signIn: async (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) throw Error('invalidCredentials');

      let Result = await UsersRepository.findUser(req.body.email);
      if (!Result) throw Error('invalidCredentials');

      let { enterprise, password_hash, ...User } = Result;
      const { client, token } = await login(
        User.id,
        req.body.password,
        password_hash,
        req.secret
      );
      res.set(resHeaders(User.email, client, token));
      return res.status(200).json(resBody(User, enterprise));
    } catch (error) {
      error.status = 401;
      return next(error);
    }
  }
};
