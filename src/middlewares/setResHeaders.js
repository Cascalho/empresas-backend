exports.setResHeaders = (req, res, next) => {
  const required = ['uid', 'client', 'access-token'];
  required.map(header => {
    if (!(header in req.headers)) return next(Error('unAuthorizedRequest'));
  });
  res.set({
    uid: req.headers.uid,
    client: req.headers.client,
    'access-token': req.headers['access-token']
  });
  next();
};
