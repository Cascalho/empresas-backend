exports.secret = (req, res, next) => {
  req.secret = process.env.SECRET || 'S3rv3R_p455_s3cr3T';
  next();
};
