const errors = {
  invalidCredentials: 'Invalid login credentials. Please try again.',
  unAuthorizedRequest: 'You need to sign in or sign up before continuing.'
};

exports.errorHandler = (err, req, res, next) => {
  console.error(err);
  if (!('status' in err)) err.status = 500;
  return res.status(err.status).json({
    success: false,
    errors: [
      errors[err.message] ? errors[err.message] : 'Internal Server Error'
    ]
  });
};
