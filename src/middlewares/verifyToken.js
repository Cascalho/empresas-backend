const jwt = require('jsonwebtoken');

exports.verifyToken = (req, res, next) => {
  try {
    jwt.verify(req.headers['access-token'], req.secret);
    next();
  } catch (error) {
    error = new Error('unAuthorizedRequest');
    error.status = 401;
    return next(error);
  }
};
