const server = require('./config/app');
const PORT = process.env.PORT || 4223;

server.listen(PORT, () =>
  console.log(`> server running at http://localhost:${PORT}`)
);
