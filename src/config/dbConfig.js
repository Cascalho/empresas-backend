require('dotenv').config();

const DIALECT = 'postgres';

module.exports = {
  development: {
    url: process.env.DEV_DATABASE_URL,
    dialect: DIALECT
  },
  test: {
    url: process.env.TEST_DATABASE_URL,
    dialect: DIALECT
  },
  production: {
    url: process.env.DATABASE_URL,
    dialect: DIALECT
  }
};
