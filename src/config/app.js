const express = require('express');
const app = express();
const cors = require('cors');
const helmet = require('helmet');
const routes = require('../routes');
const { errorHandler } = require('../middlewares/errorHandler');
const { secret } = require('../middlewares/secret');

app.use(cors());
app.use(helmet());
app.use(express.json());

app.all('/api/*', secret);
app.use('/api/v1', routes);

app.use(errorHandler);

module.exports = app;
