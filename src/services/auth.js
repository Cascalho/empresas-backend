const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');

const genJWT = (userId, client, secret) => {
  return new Promise((resolve, reject) => {
    const payload = {
      id: uuidv4(),
      iss: process.env.ISSUER_ID,
      aud: client,
      sub: userId,
      iat: Date.now(),
      expiresIn: '2d'
    };
    jwt.sign(payload, secret, (error, token) => {
      if (error) reject(error);
      resolve(token);
    });
  });
};

exports.login = async (userId, password, password_hash, secret) => {
  try {
    let match = await bcrypt.compare(password, password_hash);
    if (!match) throw Error('invalidCredentials');

    const client = uuidv4();
    return {
      client: client,
      token: await genJWT(userId, client, secret)
    };
  } catch (error) {
    throw error;
  }
};
