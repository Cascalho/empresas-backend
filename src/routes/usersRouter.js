const routes = require('express').Router();
const { check } = require('express-validator');

const UsersController = require('../controllers/UsersController');

routes.post(
  '/auth/sign_in',
  [
    check('email').notEmpty(),
    check('email').isEmail(),
    check('password').notEmpty()
  ],
  UsersController.signIn
);

module.exports = routes;
