const routes = require('express').Router();

const { verifyToken } = require('../middlewares/verifyToken');
const { setResHeaders } = require('../middlewares/setResHeaders');

const usersRoutes = require('./usersRouter');
const enterprisesRoutes = require('./enterprisesRouter');

routes.use('/users', usersRoutes);
routes.use('/enterprises', [verifyToken, setResHeaders], enterprisesRoutes);

module.exports = routes;
