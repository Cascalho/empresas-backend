const routes = require('express').Router();

const EnterprisesController = require('../controllers/EnterprisesController');

routes.get('/', EnterprisesController.index);
routes.get('/:id', EnterprisesController.show);

module.exports = routes;
