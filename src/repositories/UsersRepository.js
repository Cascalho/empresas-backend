const { User } = require('../models');

const presentResult = User => {
  let {
    enterprises,
    enterprises_number,
    createdAt,
    updatedAt,
    ...resultObj
  } = User.dataValues;
  resultObj.portfolio = {
    enterprises_number: enterprises_number,
    enterprises: enterprises.split('')
  };
  return resultObj;
};

exports.findUser = async email => {
  let result = await User.findOne({ where: { email: email } });
  if (!result) return null;

  return presentResult(result);
};
