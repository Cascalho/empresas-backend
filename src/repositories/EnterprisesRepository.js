const { Enterprise } = require('../models');

const presentResult = Enterprise => {
  let {
    enterprise_type_id,
    enterprise_type_name,
    ...resultObj
  } = Enterprise.dataValues;
  resultObj.enterprise_type = {
    id: enterprise_type_id,
    enterprise_type_name: enterprise_type_name
  };
  return resultObj;
};

const prepareQuery = queryArgs => {
  if ('city' in queryArgs) queryArgs.city = queryArgs.city.toLowerCase();
  if ('enterprise_types' in queryArgs)
    queryArgs.enterprise_type_id = queryArgs.enterprise_types;
  if ('name' in queryArgs)
    queryArgs.enterprise_name = queryArgs.name.toLowerCase();
  if ('country' in queryArgs)
    queryArgs.country = queryArgs.country.toLowerCase();
  if ('enterprise_type_name' in queryArgs)
    queryArgs.enterprise_type_name = queryArgs.enterprise_type_name.toLowerCase();
  delete queryArgs.enterprise_types;
  delete queryArgs.name;
  return queryArgs;
};

module.exports = {
  getAll: async () => {
    let result = await Enterprise.findAll();
    if (!result) return null;

    result = result.map(Enterprise => presentResult(Enterprise));
    return result;
  },

  getOne: async id => {
    let result = await Enterprise.findOne({ where: { id: id } });
    if (!result) return null;

    return presentResult(result);
  },

  filter: async queryArgs => {
    console.log('TCL: queryArgs before preparation', queryArgs);
    queryArgs = prepareQuery(queryArgs);
    console.log('TCL: queryArgs', queryArgs);
    let query = { where: { ...queryArgs } };

    console.log('TCL: query', query);
    let result = await Enterprise.findAll(query);
    console.log('TCL: result', result);

    result = result.map(Enterprise => presentResult(Enterprise));
    return result;
  }
};
