'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      investor_name: {
        type: Sequelize.STRING
      },
      email: {
        allowNull: false,
        unique: true,
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },
      balance: {
        defaultValue: 0,
        type: Sequelize.INTEGER
      },
      photo: {
        type: Sequelize.STRING
      },
      first_access: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      super_angel: {
        allowNull: false,
        defaultValue: true,
        type: Sequelize.BOOLEAN
      },
      portfolio_value: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      enterprises_number: {
        type: Sequelize.INTEGER
      },
      enterprises: {
        type: Sequelize.STRING
      },
      enterprise: {
        allowNull: true,
        type: Sequelize.STRING
      },
      password_hash: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};
