'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Enterprises', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email_enterprise: {
        type: Sequelize.STRING
      },
      facebook: {
        type: Sequelize.STRING
      },
      twitter: {
        type: Sequelize.STRING
      },
      linkedin: {
        type: Sequelize.STRING
      },
      phone: {
        type: Sequelize.STRING
      },
      own_enterprise: {
        type: Sequelize.BOOLEAN
      },
      enterprise_name: {
        type: Sequelize.STRING
      },
      photo: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      city: {
        type: Sequelize.STRING
      },
      country: {
        type: Sequelize.STRING
      },
      value: {
        type: Sequelize.INTEGER
      },
      share_price: {
        type: Sequelize.INTEGER
      },
      enterprise_type_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      enterprise_type_name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("(now() at time zone 'utc')")
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Enterprises');
  }
};
