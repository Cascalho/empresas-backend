'use strict';

let enterprises = require('../data/enterprises.json');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Enterprises', enterprises);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Enterprises', null, {});
  }
};
