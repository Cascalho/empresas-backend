'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Users',
      [
        {
          investor_name: 'Teste Apple',
          email: 'testeapple@ioasys.com.br',
          city: 'BH',
          country: 'Brasil',
          balance: 350000,
          photo: '/uploads/investor/photo/1/cropped4991818370070749122.jpg',
          enterprises_number: 0,
          enterprises: '',
          portfolio_value: 350000,
          first_access: false,
          super_angel: false,
          enterprise: null,
          password_hash:
            '$2b$15$X..fcvgwzcKwY2.ZBl4p7e7EzrAHSpABvepB5d.m77maQ6JoVYf8y'
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
