const request = require('supertest');
const api = require('../../src/config/app');
const ExpectedResult = require('../__mocks__/User');

describe('POST /users/auth/sign_in', () => {
  let accessData = { email: '', password: '' };
  const expectedError = ['Invalid login credentials. Please try again.'];

  context('when email or password is empty', () => {
    it('returns Unauthorized', async done => {
      const response = await request(api)
        .post('/api/v1/users/auth/sign_in')
        .set('Content-type', 'application/json')
        .send(accessData);

      expect(response.status).toBe(401);
      expect(response.body.success).toBeFalsy;
      expect(response.body.errors).toEqual(expectedError);
      done();
    });
  });

  context('when email or password is invalid', () => {
    it('returns Unauthorized', async done => {
      accessData = {
        email: 'invalid@email.wrong',
        password: 'invalid_password'
      };

      const response = await request(api)
        .post('/api/v1/users/auth/sign_in')
        .set('Content-type', 'application/json')
        .send(accessData);

      expect(response.status).toBe(401);
      expect(response.body.success).toBeFalsy;
      expect(response.body.errors).toEqual(expectedError);
      done();
    });
  });

  context('when email and password are valid', () => {
    it('returns uid, client and access token', async done => {
      accessData = {
        email: 'testeapple@ioasys.com.br',
        password: '12341234'
      };

      const response = await request(api)
        .post('/api/v1/users/auth/sign_in')
        .set('Content-type', 'application/json')
        .send(accessData);

      expect(response.status).toBe(200);
      expect(response.header).toHaveProperty('uid');
      expect(response.header.uid).toBeDefined();
      expect(response.header).toHaveProperty('client');
      expect(response.header.client).toBeDefined();
      expect(response.header).toHaveProperty('access-token');
      expect(response.header['access-token']).toBeDefined();
      expect(response.body.success).toBeTruthy;
      expect(response.body.investor).toEqual(ExpectedResult);
      expect(response.body.enterprise).toBeNull();
      done();
    });
  });
});
