# Notas

## pt-br

A geração de JWT e o uso do bcrypt estão um pouco lentos na minha máquina, contudo devido ao fato de que isso significa que para quebrar ambas seria necessário mais poder computacional, eu implementei dessa forma buscando segurança e simplicidade.

## en

Some executions, like the JWT generation and the bcrypt processing of the passwords are somewhat slow, cause of the computer required power to work the hashes.
I implemented it this way for simplicity and security.
